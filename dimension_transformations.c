#include <iostream>
#include <string>
#include "UnparseFormat.h"

using namespace std;
using namespace SageInterface;
using namespace SageBuilder;

typedef SgLocatedNode* SynthesizedAttribute;
typedef int InheritedAttribute;

SgProject *project_link = NULL;

class GlobalTraversal: public AstSimpleProcessing {
public:
	virtual void visit(SgNode* n);
};

class LocalTraversal: public AstTopDownBottomUpProcessing<InheritedAttribute, SynthesizedAttribute> {
	int array_dimensions;
public:
	LocalTraversal(): array_dimensions(0) {}
	int get_dimensions() {return array_dimensions;}
	virtual SynthesizedAttribute evaluateSynthesizedAttribute(SgNode* n, InheritedAttribute parent_attr, SynthesizedAttributesList child_attr);
	virtual InheritedAttribute evaluateInheritedAttribute(SgNode* n, InheritedAttribute parent_attr);
};

SynthesizedAttribute LocalTraversal::evaluateSynthesizedAttribute(SgNode* n, InheritedAttribute parent_attr, SynthesizedAttributesList child_attr)
{
	clog << "synthesized attribute at " << n->sage_class_name() << endl;
	if (child_attr.size() == 2 && child_attr[0] && child_attr[1]) {
		SgPntrArrRefExp *ref = isSgPntrArrRefExp(child_attr[0]);
		if (!ref)
			throw string("Left child sent ") + string(child_attr[0] ? child_attr[0]->sage_class_name() : NULL) + string(" instead of SgPntrArrRefExp");

		SgMultiplyOp *mult = isSgMultiplyOp(child_attr[1]);
		if (!mult)
			throw string("Right child sent ") + string(child_attr[0] ? child_attr[0]->sage_class_name() : NULL) + string(" instead of SgMultiplyOp");

		// get the contents of square brackets
		clog << "traversal successors: " << ref->get_numberOfTraversalSuccessors() << endl;
		SgVarRefExp *variable = isSgVarRefExp(ref->get_traversalSuccessorByIndex(0));
		SgNode *shift0 = ref->get_traversalSuccessorByIndex(1);
		if (!shift0) {
			// we have zero shift now
			clog << "building a new ref with multiplication in it" << endl;
			SgPntrArrRefExp *new_ref = buildPntrArrRefExp(variable, mult);
			return new_ref;
		}
		// FIX: this one is redundant, check conditions
		SgMultiplyOp *shift1 = isSgMultiplyOp(ref->get_traversalSuccessorByIndex(1));
		if (shift1) {
			clog << "building a new ref with addition in it" << endl;
			SgAddOp *new_add = buildAddOp(shift1, mult);
			SgPntrArrRefExp *new_ref = buildPntrArrRefExp(variable, new_add);
			return new_ref;
		}
		SgAddOp *shift2 = isSgAddOp(ref->get_traversalSuccessorByIndex(1));
		if (shift2) {
			clog << "building a new ref with long addition in it" << endl;
			SgAddOp *new_add = buildAddOp(shift2, mult);
			SgPntrArrRefExp *new_ref = buildPntrArrRefExp(variable, new_add);
			return new_ref;
		}
		throw string("something unexpected(") + string(ref->get_traversalSuccessorByIndex(1)->sage_class_name()) + string(") in brackets of passed to us ArrayRef");
	}

	// if we are below sent branch, return NULL
	if (parent_attr != 1) {
		clog << "skipping node" << endl;
		return NULL;
	} else {
		clog << "sending node up" << endl;
		SgVarRefExp *var = isSgVarRefExp(n);
		if (var) {
			clog << "building new reference object" << endl;
			SgPntrArrRefExp *new_ref = buildPntrArrRefExp(var);
			return new_ref;
		}
		SgExpression *tmp = isSgExpression(n);
		if (tmp) {
			return buildMultiplyOp(buildIntVal(11), tmp);
		}
		throw string("Not a node, not an expression. What is it?");
	}
}

InheritedAttribute LocalTraversal::evaluateInheritedAttribute(SgNode* n, InheritedAttribute parent_attr)
{
	clog << "inherited attribute at " << n->sage_class_name() << endl;
	if (isSgPntrArrRefExp(n) && (!array_dimensions || !parent_attr && (n == n->get_parent()->get_traversalSuccessorByIndex(0)))) {
		clog << "setting 0 as inh_attr" << endl;
		array_dimensions++;
		return 0;
	} else {
		// if we are on the right part, pass the entire branch
		clog << "setting " << parent_attr + 1 << " as inh_attr" << endl;
		return parent_attr + 1;
	}
}

void GlobalTraversal::visit(SgNode* n) 
{
	// NodeQuery::querySubTree should be used instead of this
	SgPntrArrRefExp* ptr_assignment = isSgPntrArrRefExp(n);
	if (ptr_assignment != NULL) {
		LocalTraversal local_traversal;
		SgLocatedNode *dimensions = local_traversal.traverse(n, 0);
		clog << "dimensions: " << local_traversal.get_dimensions() << endl;
		if (local_traversal.get_dimensions() > 1) {
			SgExpression *new_exp = deepCopy(isSgExpression(dimensions));
			if (new_exp) {
				replaceExpression(ptr_assignment, new_exp);
			}
			throw string();
		}
	}
}

int main(int argc, char * argv[])
{
	SgProject* project = project_link = frontend(argc,argv);

	for (;;) {
		try {
			GlobalTraversal exampleTraversal;
			exampleTraversal.traverseInputFiles(project, preorder);
			break;
		} catch (string s) {
			// project->unparse();
			// sleep(5);
		}
	}

	UnparseFormatHelp *f = new UnparseFormatCustom();
	project->unparse(f);
	return 0; 
}

