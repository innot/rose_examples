include Makefile.local

target = identityTranslator
file = t_$(target).c

all: $(target)

run: $(target)
	./$(target) $(file) && echo && echo ">>>>> source:" && cat $(file) && echo && echo ">>>>> results:" && cat rose_$(file)

$(target).c:
	rm -f $(target).o $(target).lo .libs/$(target).o $(target)

UnparseFormat.cpp:
	rm -f UnparseFormat.o UnparseFormat.lo  .libs/UnparseFormat.o

UnparseFormat.lo: UnparseFormat.cpp
	libtool --mode=compile --tag=CXX $(CXX) $(CXXFLAGS)  $(CPPFLAGS) -I$(ROSE_INCLUDE_DIR) $(BOOST_CPPFLAGS) -c -o UnparseFormat.lo $(ROSE_SOURCE_DIR)/UnparseFormat.cpp

$(target).lo: $(target).c
	libtool --mode=compile --tag=CXX $(CXX) $(CXXFLAGS)  $(CPPFLAGS) -I$(ROSE_INCLUDE_DIR) $(BOOST_CPPFLAGS) -c -o $(target).lo $(ROSE_SOURCE_DIR)/$(target).c

$(target): $(target).lo UnparseFormat.lo
	libtool --mode=link --tag=CXX $(CXX) $(CXXFLAGS) $(LDFLAGS) -o $(target) $(target).lo UnparseFormat.lo $(ROSE_LIBS)

clean:
	rm $(target).o $(target).lo $(target) .libs/$(target).o

cleanall:
	rm -f *.o *.lo .libs/*.o
	rm -f *.pdf
	find ./ -maxdepth 1 -type f -perm -o+rx | xargs rm -f
